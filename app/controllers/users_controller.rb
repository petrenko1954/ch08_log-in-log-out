#Листинг 7.5: Контроллер Users с действием show. app/controllers/users_controller.rb Листинг 7.6: Контроллер Users с отладчиком. app/controllers/users_controller.rb  Листинг 7.12: Добавление переменной @user к действию new. app/controllers/users_controller.rb Листинг 7.16: Действие create, которое может обрабатывать провальную регистрацию. app/controllers/users_controller.rb Листинг 7.17: Использование строгих параметров в действии create. app/controllers/users_controller.rb Листинг 7.23: Действие create с сохранением и перенаправлением. app/controllers/users_controller.rb Листинг 7.24: Добавление флэш-сообщения к регистрации пользователя. app/controllers/users_controller.rb Листинг 8.22: Вход пользователя после регистрации. app/controllers/users_controller.rb 

class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])

  end

  def new
@user = User.new
end
def create
    @user = User.new(user_params)
    if @user.save
      log_in @user

 flash[:success] = "Welcome to the Sample App!"
           redirect_to @user

    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
end
